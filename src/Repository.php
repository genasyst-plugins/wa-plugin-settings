<?php

namespace Genasyst\WaPluginSettings;

use Genasyst\Webasyst\Database\Repository as WebasystRepository;

/**
 * Class Repository
 * @package Genasyst\WaShopHelpers\Plugin\Settings
 * @throws
 */
class  Repository extends WebasystRepository implements RepositoryInterface
{

    /**
     * @param null $key
     *
     * @return array|null|string
     */
    public function get($key = null)
    {
        if (!is_null($key)) {
            $data = $this->getModel()->getByField(
                $this->prepareQueryData(array(
                    'key' => $key
                ))
            );
            if (!empty($data)) {
                return $data['value'];
            }
            return null;
        } else {
            $data = $this->getModel()->getByField($this->prepareQueryData([]), true);
            $return = array();
            foreach ($data as $v) {
                $return[$v['key']] = $v['value'];
            }
            return $return;
        }
    }


    public function set($key, $values = null)
    {
        if (is_null($values) && is_array($key)) {
            $insert = array();
            $settings = $this->get();
            if (!is_array($settings)) {
                $settings = array();
            }
            foreach ($key as $k => $v) {
                if (array_key_exists($k, $settings)) {
                    $this->getModel()->updateByField(
                        $this->prepareQueryData(array(
                        'key' => $k
                    )), array('value' => $v));
                } else {
                    $insert[] = $this->prepareQueryData(array(
                                    'key' => $k,
                                    'value' => $v
                                ));
                }
                unset($settings[$k]);
            }
            if (!empty($settings)) {
                foreach ($settings as $k => $val) {
                    $this->getModel()->deleteByField(
                        $this->prepareQueryData(array(
                            'key' => $k
                        ))
                    );
                }
            }
            if (!empty($insert)) {
                $this->getModel()->multipleInsert($insert);
            }
            return true;
        } elseif (!is_array($key) && !is_null($values)) {
            $setting = $this->get($key);
            if (!empty($setting)) {
                $this->getModel()->updateByField(
                    $this->prepareQueryData(array(
                        'key' => $key
                    )),
                    array('value' => $values));
            } else {
                $this->getModel()->insert(
                    $this->prepareQueryData(array(
                        'key' => $key,
                        'value' => $values
                    ))
                );
            }
        }
    }

    protected function prepareQueryData(array $data)
    {
        return $data;
    }
}

