<?php


namespace Genasyst\WaPluginSettings;

use ArrayAccess;
use Genasyst\Webasyst\Database\Model;

abstract class SettingsStorage implements ArrayAccess
{

    /**
     * Модель для работы вс данными объекта настроек
     * @var null
     */
    protected $repository = null;

    /**
     * Данные настроек
     * @var array
     */
    protected $data = array();

    /**
     * Название table
     * @var string
     */
    protected $table_name = '';

    /**
     * Массив настроек по умолчанию
     * @var array
     */
    protected $default_settings = array();

    /**
     *
     */
    public function __construct()
    {
        $this->initRepository();
        $this->initSettings();
        $this->init();
    }

    /**
     * Метод инициализирует массив настроек для текущей витрины
     * @see $this->data
     */
    protected function initSettings()
    {
        $data = $this->getRepository()->get();
        if (!empty($data)) {
            $this->data = $data;
        }
    }
    /**
     * Метод инициализирует массив настроек для текущей витрины
     * @see $this->data
     */
    protected function initRepository()
    {
        if ($this->repository == null) {
            $this->repository = new Repository(new Model($this->table_name));
        }
        return $this->repository;
    }

    /**
     * Метод инициализации объекта, необязательный
     */
    protected function init()
    {
    }

    /**
     * Возвращает настройку по умолчанию или весь массив настроек
     * @param null $name
     * @return null
     */
    public function getDefault($name = null)
    {
        if ($name === null) {
            return $this->default_settings;
        }
        if (isset($this->default_settings[$name])) {
            return $this->default_settings[$name];
        }
        return null;
    }

    /**
     * Возвращает модель настроек объекта
     * @return  Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }


    /**
     * Метод сохранения настроек плагина
     * @param $data - массив ключей и значений настроек для витрины
     * @return mixed
     */
    abstract public function save($data);

    /* Стандартные методы интерфейса ArrayAccess */

    /**
     * @param string $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return (array_key_exists($offset, $this->data)
            || array_key_exists($offset, $this->default_settings)
        );

    }

    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return $this->data[$offset];
        } elseif (array_key_exists($offset, $this->default_settings)) {
            return $this->default_settings[$offset];
        }
        return null;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

}