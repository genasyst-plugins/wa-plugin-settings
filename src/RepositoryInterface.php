<?php


namespace Genasyst\WaPluginSettings;


interface RepositoryInterface
{
    /**
     * @param null|string $key
     * @return mixed
     */
    public function get($key = null);

    /**
     * @param string|array $key
     * @param null|string $values
     * @return mixed
     */
    public function set($key, $values = null);
}

